package com.epam.jwd.task1.java.main;

import com.epam.jwd.task1.java.comparator.StyleComparator;
import com.epam.jwd.task1.java.entity.AudioTrack;
import com.epam.jwd.task1.java.entity.MusicGenre;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        StyleComparator styleComparator = new StyleComparator();
        ArrayList<AudioTrack> disk = new ArrayList<>();
        disk.add(new AudioTrack("Treasure chest party quest",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 256, true));
        disk.add(new AudioTrack("DEUTSCHLAND",
                "RAMMSTEIN", "Rammstein",
                MusicGenre.INDUSTRIALMETAL, 322, false));
        disk.add(new AudioTrack("Fannybaws",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 254, true));
        disk.add(new AudioTrack("Geiger Counter / Radioactivity",
                "3-D The Catalogue", "Kraftwerk",
                MusicGenre.ELECTRO, 405, false));
        disk.add(new AudioTrack("Chomp Chomp",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 212, true));
        disk.add(new AudioTrack("Gangsta's Paradise",
                "Gangsta's Paradise", "Coolio",
                MusicGenre.RAP, 240, false));
        disk.add(new AudioTrack("Tortuga",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 202, false));
        disk.add(new AudioTrack("Defence of Moscow",
                "Defence of Moscow", "Sabaton",
                MusicGenre.POWERMETAL, 248, false));
        disk.add(new AudioTrack("Zombies Ate My Pirate Ship",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 304, true));
        disk.add(new AudioTrack("Дурак и молния",
                "Камнем по голове", "Король и Шут",
                MusicGenre.PUNKROCK, 116, false));
        disk.add(new AudioTrack("Call of the Waves",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 305, false));
        disk.add(new AudioTrack("Heresy",
                "Cowboys From Hell", "Pantera",
                MusicGenre.HEAVYMETAL, 284, true));
        disk.add(new AudioTrack("Pirate's Scorn",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 167, false));
        disk.add(new AudioTrack("OK2BGAY",
                "OK2BGAY", "Anthony McBazooka",
                MusicGenre.POP, 288, false));
        disk.add(new AudioTrack("Shit Boat (No Fans)",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 74, true));
        disk.add(new AudioTrack("Письмо в редакцию телевизионной передачи " +
                "\"Очевидное-невероятное\" из сумасшедшего дома - с Канатчиковой дачи",
                "Шуточные и сатирические песни", "Владимир Высоцкий",
                MusicGenre.BALLAD, 382, false));
        disk.add(new AudioTrack("Pirate Metal Drinking Crew",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 224, true));
        disk.add(new AudioTrack("Летела речка",
                "Мы вышли покурить на 17 лет", "Михаил Елизаров",
                MusicGenre.CHANSON, 162, true));
        disk.add(new AudioTrack("Wooden Leg Part 2 (The Woodening)",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 486, false));
        disk.add(new AudioTrack("Land in Sicht",
                "Bis ans Ende der Welt", "Santiano",
                MusicGenre.MUSICOFTHEWORLD, 256, false));
        disk.add(new AudioTrack("Henry Martin",
                "Curse of the Crystal Coconut", "Alestorm",
                MusicGenre.PIRATEMETAL, 149, false));
        System.out.println("Before sort:");
        System.out.println(disk);
        System.out.println();
        Collections.sort(disk, styleComparator);
        System.out.println("After sort:");
        System.out.println(disk);
    }
}
