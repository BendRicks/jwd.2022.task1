package com.epam.jwd.task1.java.entity;

public class MediaContent {

    protected String name;
    protected String author;
    protected boolean nsfw;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isNsfw() {
        return nsfw;
    }

    public void setNsfw(boolean nsfw) {
        this.nsfw = nsfw;
    }
}
