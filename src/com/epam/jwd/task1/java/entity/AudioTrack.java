package com.epam.jwd.task1.java.entity;

public class AudioTrack extends MediaContent implements Comparable<AudioTrack>{

    private MusicGenre genre;
    private String album;
    private int songLength;

    public AudioTrack(String name, String album, String author, MusicGenre genre, int songLength, boolean nsfw){
        this.nsfw = nsfw;
        this.name = name;
        this.author = author;
        this.album = album;
        this.genre = genre;
        this.songLength = songLength;
    }

    public MusicGenre getGenre() {
        return genre;
    }

    public int getSongLength() {
        return songLength;
    }

    public String getAlbum() {
        return album;
    }

    @Override
    public int compareTo(AudioTrack audioTrack){
        return this.genre.getName().compareTo(audioTrack.genre.getName());
    }

    @Override
    public String toString(){
        return "Song \"" + this.name + "\" from album \""
                + this.album + "\" by \"" + this.author
                + "\". Genre - " + this.genre.getName()
                + ". Length - " + this.songLength + " sec"
                + (this.nsfw ? ". NSFW." : ".") + "\n";
    }

    @Override
    public int hashCode(){
        return this.name.hashCode() + this.author.hashCode()
                + this.album.hashCode() + this.genre.hashCode()
                + (this.nsfw ? 1 : 0) + this.songLength;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) { return true; }
        if (o.getClass() != this.getClass()) { return false; }
        AudioTrack audioTrack = (AudioTrack) o;
        return audioTrack.name.equals(this.name) &&
                audioTrack.author.equals(this.author) &&
                audioTrack.album.equals(this.album) &&
                (audioTrack.nsfw == this.nsfw) &&
                (audioTrack.genre == this.genre) &&
                (audioTrack.songLength == this.songLength);

    }
}