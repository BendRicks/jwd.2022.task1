package com.epam.jwd.task1.java.entity;

public enum MusicGenre {
    UNKNOWN("Unknown"),
    ROCK("Rock"),
    HARDROCK("Hard Rock"),
    PUNKROCK("Punk Rock"),
    POP("Pop"),
    HEAVYMETAL("Heavy Metal"),
    PROGRESSIVEMETAL("Progressive Metal"),
    INDUSTRIALMETAL("Industrial Metal"),
    POWERMETAL("Power Metal"),
    GRUNGEMETAL("Grunge Metal"),
    PIRATEMETAL("Pirate Metal"),
    JAZZ("Jazz"),
    BLUES("Blues"),
    CHANSON("Chanson"),
    BALLAD("Ballad"),
    ELECTRO("Electro"),
    RAP("Rap"),
    MUSICOFTHEWORLD("Music of the World");
    private String genreName;
    MusicGenre(String genreName){
        this.genreName = genreName;
    }
    public String getName(){
        return this.genreName;
    }
}