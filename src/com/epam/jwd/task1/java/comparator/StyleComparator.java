package com.epam.jwd.task1.java.comparator;

import com.epam.jwd.task1.java.entity.AudioTrack;

import java.util.Comparator;

public class StyleComparator implements Comparator<AudioTrack> {


    @Override
    public int compare(AudioTrack o1, AudioTrack o2) {
        return o1.compareTo(o2);
    }
}
